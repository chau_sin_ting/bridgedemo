package com.bridgedemo.NativeComponent;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.bridgedemo.R;
import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.ReactContext;
import com.facebook.react.bridge.WritableMap;
import com.facebook.react.uimanager.events.RCTEventEmitter;

/**
 * Created by user on 28/9/2018.
 */

public class NativeComponentView extends LinearLayout {
    private Button btn;
    private EditText et;
    public static final String EVENT = "onSubmit";

    public NativeComponentView(Context context) {
        super(context);
        init();
    }

    public NativeComponentView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public NativeComponentView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        View.inflate(getContext(), R.layout.view_native_component, this);
        connectView();
        setListeners();
    }

    private void connectView() {
        btn = findViewById(R.id.btn);
        et = findViewById(R.id.et);
    }

    private void setListeners() {
        btn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                WritableMap event = Arguments.createMap();
                event.putString("text", et.getText().toString());
                ReactContext context = (ReactContext) getContext();
                context.getJSModule(RCTEventEmitter.class).receiveEvent(getId(), EVENT, event);
            }
        });
    }


    public void setHints(@Nullable String text) {
        et.setHint(text);
    }

    public void setButtonText(@Nullable String text) {
        btn.setText(text);
    }

    public void clearText() {
        et.setText("");
    }

}
