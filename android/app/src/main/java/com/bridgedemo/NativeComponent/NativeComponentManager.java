package com.bridgedemo.NativeComponent;

import android.widget.Button;

import com.facebook.react.common.MapBuilder;
import com.facebook.react.uimanager.SimpleViewManager;
import com.facebook.react.uimanager.ThemedReactContext;
import com.facebook.react.uimanager.annotations.ReactProp;

import java.util.Map;

import javax.annotation.Nullable;

/**
 * Created by user on 27/9/2018.
 */

public class NativeComponentManager extends SimpleViewManager<NativeComponentView> {

    public static final String REACT_CLASS = "NativeComponentView";
    public static final String EVENT = "onSubmit";
    public static NativeComponentView nativeComponentView;

    @Override
    public String getName() {
        return REACT_CLASS;
    }

    @Override
    public NativeComponentView createViewInstance(ThemedReactContext context) {
        nativeComponentView = new NativeComponentView(context);
        return nativeComponentView;
    }

    @ReactProp(name = "hints")
    public void setHints(NativeComponentView view, @Nullable String text) {
        view.setHints(text);
    }

    @ReactProp(name = "buttonText")
    public void setButtonText(NativeComponentView view, @Nullable String text) {
        view.setButtonText(text);
    }

    @Nullable
    @Override
    public Map<String, Object> getExportedCustomDirectEventTypeConstants() {
        MapBuilder.Builder<String, Object> builder = MapBuilder.builder();
        builder.put(EVENT, MapBuilder.of("registrationName", EVENT));
        return builder.build();
    }
}