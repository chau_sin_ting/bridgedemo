package com.bridgedemo.NativeComponent;

import android.graphics.Color;

import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;

/**
 * Created by user on 27/9/2018.
 */

public class NativeComponentModule extends ReactContextBaseJavaModule {
    public static final String REACT_CLASS = "NativeComponentModule";
    private int bgColor;

    public NativeComponentModule(ReactApplicationContext reactContext) {
        super(reactContext);
    }

    @Override
    public String getName() {
        return REACT_CLASS;
    }

    @ReactMethod
    public void clearText() {
        if (NativeComponentManager.nativeComponentView != null) {
            NativeComponentManager.nativeComponentView.clearText();
        }
    }

}
