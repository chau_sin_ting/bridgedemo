import PropTypes from 'prop-types'
import { View, requireNativeComponent } from 'react-native'

var iface = {
  name: 'NativeComponent',
  propTypes: {
    buttonText: PropTypes.string,
    hints: PropTypes.string,
    onSubmit: PropTypes.func,
  }
}

const NativeComponentView = requireNativeComponent('NativeComponent', iface)
export default NativeComponentView