import PropTypes from 'prop-types'
import { View, requireNativeComponent } from 'react-native'

var iface = {
  name: 'NativeComponentView',
  propTypes: {
    buttonText: PropTypes.string,
    hints: PropTypes.string,
    onSubmit: PropTypes.func,
  }
}

const NativeComponentView = requireNativeComponent('NativeComponentView', iface)
export default NativeComponentView