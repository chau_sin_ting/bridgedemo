/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react'
import { Dimensions, Platform, StyleSheet, Image, View, NativeModules, TouchableOpacity } from 'react-native'
import NativeComponentView from './NativeComponent'
import resolveAssetSource from 'resolveAssetSource'


type State = {
  url: ?string
}

export default class App extends Component<Props,State> {

  state = {
    url : ''
  }

  onPressed = () => {
    NativeModules.NativeComponentModule.clearText()
  }

  onSubmit = (event: Event )=>{
    this.setState({
      url: event.nativeEvent.text
    })
  }
  
  render() {
    const bears = require('./Images/bears.jpg')
    const source = this.state.url.length > 0 ?
    { uri: this.state.url } : bears 
    return (
      <View style={styles.container}>
        <NativeComponentView
          style={styles.component}
          hints={'Image Url'}
          buttonText={'Enter'}
          onSubmit={this.onSubmit}
        />
        <TouchableOpacity
        onPress={this.onPressed}
        >
        <Image
          style={styles.image}
          defaultSource={bears}
          source={source}
        />
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: '#F5FCFF'
  },
  component: {
    width: Dimensions.get('window').width,
    height: 100,
    justifyContent: 'center',
    alignItems: 'center'
  },
  image:{
    width:300,
    height:300,
    marginTop:20,
    justifyContent: 'center'
  }
});
