//
//  NativeComponentView.m
//  BridgeDemo
//
//  Created by RickyChow on 28/9/2018.
//  Copyright © 2018 Facebook. All rights reserved.
//

#import "NativeComponentView.h"

@implementation NativeComponentView

- (void)layoutSubviews {
  [super layoutSubviews];
  UIView *componentView = [[[NSBundle mainBundle] loadNibNamed:@"NativeComponentView" owner:self options:nil] firstObject];
  self.textField.placeholder = self.hints;
  [self.submitBtn setTitle: self.buttonText forState:UIControlStateNormal];
  [self addSubview:componentView];
}

- (IBAction)submitBtnOnPress:(UIButton *)sender {
  if(self.textField.text){
    self.onSubmit(@{@"text": self.textField.text});
  }
}

-(void)clearText{
    [self.textField setText:@""];
}


@end
