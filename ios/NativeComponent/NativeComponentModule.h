//
//  NativeComponentModule.h
//  BridgeDemo
//
//  Created by RickyChow on 1/10/2018.
//  Copyright © 2018 Facebook. All rights reserved.
//

#import <React/RCTViewManager.h>
#import <React/RCTBridgeModule.h>

@interface NativeComponentModule : RCTViewManager <RCTBridgeModule>


@end
