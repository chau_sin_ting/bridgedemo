//
//  NativeComponentManager.h
//  BridgeDemo
//
//  Created by RickyChow on 28/9/2018.
//  Copyright © 2018 Facebook. All rights reserved.
//

#import <React/RCTViewManager.h>
#import <React/RCTBridgeModule.h>
#import "NativeComponentView.h"

static NativeComponentView *nativeComponentView;

@interface NativeComponentManager : RCTViewManager <RCTBridgeModule>
+(void)clearText;
@end
