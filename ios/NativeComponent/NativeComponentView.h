//
//  NativeComponentView.h
//  BridgeDemo
//
//  Created by RickyChow on 28/9/2018.
//  Copyright © 2018 Facebook. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <React/RCTComponent.h>

@interface NativeComponentView : UIView <UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITextField *textField;
@property (weak, nonatomic) IBOutlet UIButton *submitBtn;

@property (nonatomic, strong) NSString *buttonText;
@property (nonatomic, strong) NSString *hints;
@property (nonatomic, copy) RCTBubblingEventBlock onSubmit;
-(void)clearText;
- (IBAction)submitBtnOnPress:(UIButton *)sender;
@end
