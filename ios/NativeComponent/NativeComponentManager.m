//
//  NativeComponentManager.m
//  BridgeDemo
//
//  Created by RickyChow on 28/9/2018.
//  Copyright © 2018 Facebook. All rights reserved.
//

#import "NativeComponentManager.h"
#import "NativeComponentView.h"
#import <React/UIView+React.h>

@implementation NativeComponentManager
RCT_EXPORT_MODULE()

RCT_EXPORT_VIEW_PROPERTY(buttonText, NSString)
RCT_EXPORT_VIEW_PROPERTY(hints, NSString)
RCT_EXPORT_VIEW_PROPERTY(onSubmit, RCTBubblingEventBlock)

- (UIView *)view {
  nativeComponentView = [[NativeComponentView alloc] init];
  return nativeComponentView;

}

+ (void)clearText {
    dispatch_async(dispatch_get_main_queue(), ^(){
       [nativeComponentView clearText];
      });
}

@end
