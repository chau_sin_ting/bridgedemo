//
//  NativeComponentModule.m
//  BridgeDemo
//
//  Created by RickyChow on 1/10/2018.
//  Copyright © 2018 Facebook. All rights reserved.
//

#import "NativeComponentModule.h"
#import <React/UIView+React.h>
#import "NativeComponentManager.h"

@implementation NativeComponentModule
RCT_EXPORT_MODULE()

RCT_EXPORT_METHOD(clearText)
{
    [NativeComponentManager clearText];
}

@end
